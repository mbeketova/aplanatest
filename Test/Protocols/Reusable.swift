//
//  Reusable.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import UIKit

protocol Reusable {
    static var nib: UINib {get}
    static var reuseIdentifier: String {get}
}

extension Reusable {
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static var reuseIdentifier: String {
        return "\(String(describing: self))Identifier"
    }
    
}
