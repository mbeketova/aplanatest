//
//  ViewModelProtocol.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation

protocol ViewModelProtocol {
    associatedtype T
    
    init(model: T)
}
