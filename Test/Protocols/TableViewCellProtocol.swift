//
//  TableViewCellProtocol.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import UIKit

protocol TableViewCellProtocol: class, Reusable, ConfigurableCell {
    static var height: CGFloat { get }
}

fileprivate protocol TableViewProtocol: class {
    func dequeue<T: TableViewCellProtocol>(indexPath: IndexPath) -> T
    func registerNib<T: TableViewCellProtocol>(_ type: T.Type)
}

extension UITableView: TableViewProtocol {
    func dequeue<T: TableViewCellProtocol>(indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
    
    func registerNib<T: TableViewCellProtocol>(_ type: T.Type) {
        self.register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
}

protocol ConfigurableCell {
    associatedtype T
    
    func bind(viewModel: T)
}

protocol CellLifecycleProtocol: class {
    func willDisplay()
}
