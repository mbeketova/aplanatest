//
//  Alamofire+Cancel.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Alamofire

extension SessionManager {
    func cancelTasks(for path: String) {
        sessionManager.session.getAllTasks { tasks in
            tasks.forEach{ task in
                guard task.currentRequest?.url?.path.range(of: path) != nil else { return }
                task.cancel()
            }
        }
    }
}
