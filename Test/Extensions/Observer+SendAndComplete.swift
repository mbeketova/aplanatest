//
//  Observer+SendAndComplete.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import ReactiveSwift

extension Signal.Observer {
    func complete(value: Value) {
        send(value: value)
        sendCompleted()
    }
}
