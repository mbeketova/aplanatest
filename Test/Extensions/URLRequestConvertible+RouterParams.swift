//
//  URLRequestConvertible+RouterParams.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation
import Alamofire

extension URLRequestConvertible where Self:NetworkRouterParams  {
    func asURLRequest() throws -> URLRequest {
        let baseUrl = Constants.Network.baseUrl
        let url = baseUrl.appendingPathComponent(self.path)
        var request = try! URLRequest(url: url, method: self.method, headers: self.headers)
        request.cachePolicy = .reloadIgnoringLocalCacheData
        return try self.encoding.encode(request, with: self.parameters)
    }
}
