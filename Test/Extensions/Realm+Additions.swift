//
//  Realm+Additions.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import RealmSwift
import ReactiveSwift
import Result

extension Realm {
    @discardableResult
    func create(field: Any, update: Bool = true) -> BaseValue {
        var field = field as! [String : AnyObject]
        let typeString = (field["type"] as! String).lowercased()
        let type = ValueType(rawValue: typeString)!
        switch (type) {
        case .list:
            if let concreteValue = field["values"] as? [String : AnyObject]{
                let values: [[String: String]] = concreteValue.map{ (arg) in
                    let (key, object) = arg
                    return ["key": key , "object": object as! String]
                }
                field["values"] = values as AnyObject
            }
            return self.create(ListValue.self, value: field, update: update)
        case .numeric:
            if let concreteValue = field["value"] as? Float {
                field["concreteValue"] = concreteValue as AnyObject
            }
            return self.create(NumericValue.self, value: field, update: update)
        case .text:
            if let concreteValue = field["value"] as? String {
                field["concreteValue"] = concreteValue as AnyObject
            }
            return self.create(TextValue.self, value: field, update: update)
        }
    }
}
