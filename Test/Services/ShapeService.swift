//
//  ShapeService.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Result
import RealmSwift
import ReactiveSwift

private protocol Interface {
    func meta() -> SignalProducer<Void, Error>
    func send(values: [[String:String]]) -> SignalProducer<String, Error>
}

class ShapeService: Interface {
    private let configuration: Realm.Configuration
    
    init(configuration: Realm.Configuration = Realm.instance.configuration) {
        self.configuration = configuration
    }

    func meta() -> SignalProducer<Void, Error> {
        let request = ShapeRouter.meta()
        return HttpClient.shared.load(request: request, qos: .default)
            .flatMap(.latest, save)
            .observe(on: QueueScheduler.main)
    }
    
    func send(values: [[String : String]]) -> SignalProducer<String, Error> {
        let request = ShapeRouter.data(selectedValue: values)
        return HttpClient.shared.load(request: request, qos: .default)
            .flatMap(.latest, save)
            .observe(on: QueueScheduler.main)
    }

}

//MARK: - Save
private extension ShapeService {
    func save(shape: AnyObject) -> SignalProducer<Void, NoError> {
        var shape = shape as! [String : AnyObject]
        return SignalProducer { [weak self] sink, lifetime in
            guard let sself = self else { lifetime.observeEnded{ }?.dispose(); return }
            let realm = Realm.instance(with: sself.configuration)
            try! realm.write {
                let fields = shape["fields"] as! [[String : AnyObject]]
                let newFields: [[String: AnyObject]] = fields.map{ (field) in
                    var newField = field
                    newField["eid"] = arc4random() as AnyObject
                    return newField
                }
                shape["fields"] = newFields as AnyObject
                newFields.forEach{ (field) in
                    realm.create(field: field)
                }
                realm.create(Shape.self, value: shape, update: true)

            }
           sink.complete(value: ())
        }
    }
    
    func save(result: AnyObject) -> SignalProducer<String, NoError> {
        var result = result as! [String : AnyObject]
        return SignalProducer{ sink, lifetime in
            sink.complete(value: result["result"] as! String)
        }
    }
}

