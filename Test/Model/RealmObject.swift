//
//  RealmObject.swift
//  Test
//
//  Created by Mariya on 26.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

enum CommonAttributes: String {
    case eid = "eid"
}

@objcMembers
class RealmObject : Object {
    dynamic var eid: Int = 0
    
    override class func indexedProperties() -> [String] {
        return [CommonAttributes.eid.rawValue]
    }
    
    override class func primaryKey() -> String? {
        return CommonAttributes.eid.rawValue
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? RealmObject else { return false }
        return self.eid == object.eid
    }
    
    override var hashValue: Int { return self.eid.hashValue }
}

func == <T: RealmObject>(lhs: T, rhs: T) -> Bool {
    return lhs.eid == rhs.eid
}

