//
//  Shape.swift
//  Test
//
//  Created by Mariya on 26.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class Shape: RealmObject {
    dynamic var title: String = ""
    let fields = List<Field>()
    
    override static func indexedProperties() -> [String] {
        return [CommonAttributes.eid.rawValue, "name"]
    }
}

