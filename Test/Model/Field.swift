//
//  Field.swift
//  Test
//
//  Created by Mariya on 26.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation
import RealmSwift

protocol Deserializable {
    static func normalize(json: inout [String : AnyObject])
}

extension Deserializable {
    static func normalize(json: inout [String : AnyObject]) {}
}

enum ValueType: String {
    case text = "text"
    case numeric = "numeric"
    case list = "list"
}

class Field: RealmObject {
    dynamic var title: String = ""
    dynamic var name: String = ""
    var type: ValueType {
        get {
            return ValueType(rawValue: name) ?? ValueType.text
        }
        set {
            name = newValue.rawValue
        }
    }
    var value: BaseValue?
}

@objcMembers
class BaseValue: RealmObject {
    dynamic var name: String = ""
}

@objcMembers
final class TextValue: BaseValue {
    dynamic var concreteValue: String = ""
}

@objcMembers
final class NumericValue: BaseValue {
    dynamic var concreteValue: Float = 0
}

final class ListValue: BaseValue {
    var values = List<ConcreteListValue>()
    dynamic var concreteValue: String = ""
}

@objcMembers
final class ConcreteListValue: Object {
    dynamic var key: String = ""
    dynamic var object: String = ""
}

struct SelectedFieldValue {
    let key: String
    let object: AnyObject
    
    init(key: String, object: AnyObject) {
        self.key = key
        self.object = object
    }
}

protocol AbstractSelectionField {
    func valuesArray() -> [String]
    var value: BaseValue { get }
}

extension ListValue : AbstractSelectionField {
    var value: BaseValue {
        return self
    }
    
    func valuesArray() -> [String] {
        return Array(self.values).map({$0.object})
    }
}
