//
//  Constants.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation
import RealmSwift

struct Constants {
    static let realmSchemaVersion: UInt64 = 1
    struct Network {
        static let baseUrl: URL = URL(string: "http://test.clevertec.ru/tt/")!
    }
    struct ErrorCodes {
        static let connectionLost = -1009
        static let serviceDenied = 181093
        static let corruptedData = 100500
    }
    
    struct RealmConfigurations {
        static let shape          = Realm.configuration(named: "shape")
    }
    
    struct UI {
        static let cellHeight: CGFloat = 50
        static let indicatorSize: CGSize = CGSize(width: 100, height: 100)
        static let buttonHeight: CGFloat = 60
        static let horizontalMargin: CGFloat = 15
        static let widthRightDelta: CGFloat = 50
    }
}
