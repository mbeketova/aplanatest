//
//  ShapeViewModel.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation
import RealmSwift
import ReactiveSwift

protocol ShapeViewModelDelegate: class {
    func show(error: Error)
    func show(message: String)
    func updateTableView()
}

final class ShapeViewModel {
    var results: Results<Shape>
    var title: String = ""
    var fildViewModels: [FieldViewModel] = []
    var selectedViewModel: SelectedViewModel!
    weak var delegate: ShapeViewModelDelegate?
    private var shape: Shape?
    private let service = ShapeService(configuration: Constants.RealmConfigurations.shape)
    private var disposable: Disposable?
    private var notificationToken: NotificationToken!
    private var selectedArray: [[String:String]] = []
    
    init() {
        self.results = Realm.instance(with: Constants.RealmConfigurations.shape).objects(Shape.self)
        self.shape = results.last
        setupRealmNotifications()
    }
    
    deinit {
        stopRequest()
        self.notificationToken.invalidate()
    }
}

//MARK: - SelectedViewModelDelegate
extension ShapeViewModel: SelectedViewModelDelegate {
    func selected(name: String, value: String) {
        appendNewValue(name: name, value: value)
        saveConcrete(value: value)
        self.delegate?.updateTableView()
    }
}

//MARK: - FieldViewModelDelegate
extension ShapeViewModel: FieldViewModelDelegate {
    func updateConcreteValue(name: String, value: String) {
        appendNewValue(name: name, value: value)
    }
}

//MARK: - Configure
private extension ShapeViewModel {
    func configureTitle() {
        guard let shape = self.shape else { return }
        self.title = shape.title
    }
    
    func configureFilds() {
        self.shape = self.results.last
        guard let shape = self.shape else { return }
        self.fildViewModels = shape.fields.map({ (field) -> FieldViewModel in
            if field.type == .list {
                self.configureSelectedViewModel(field: field)
            }
            let fieldViewModel = FieldViewModel(model: field)
            fieldViewModel.delegate = self
            return fieldViewModel
        })
    }
    
    func configureSelectedViewModel(field: Field) {
        let realm = Realm.instance(with: Constants.RealmConfigurations.shape)
        guard let values = realm.objects(ListValue.self).last else { return }
        let listValues = values.valuesArray()
        self.selectedViewModel = SelectedViewModel(name: field.name, values: listValues)
        self.selectedViewModel.delegate = self
    }
}

//MARK: - Help metods
private extension ShapeViewModel {
    func appendNewValue(name: String, value: String) {
        let element = [name:value]
        let oldElement = self.selectedArray.filter { $0.keys.contains(name)}.first
        let isContains = self.selectedArray.filter { $0.keys.contains(name)}.count > 0
        if isContains {
            let index = self.selectedArray.index(where: { $0 == oldElement! })
            self.selectedArray.remove(at: index!)
        }
        self.selectedArray.append(element)
    }
    
    func saveConcrete(value: String) {
        self.fildViewModels = self.fildViewModels.map { (fieldViewModel) -> FieldViewModel in
            fieldViewModel.concreteValue = value
            return fieldViewModel
        }
    }
}

//MARK: - Requests
extension ShapeViewModel {
    func loadShape() {
        self.disposable = self.service.meta().startWithResult({ [weak self] (result) in
            guard let error = result.error else { return }
            self?.delegate?.show(error: error)
        })
    }
    
    func sendValues() {
        print(self.selectedArray)
        self.disposable = self.service.send(values: self.selectedArray).startWithResult({ [weak self] (result) in
            guard let weakSelf = self else { return }
            guard let error = result.error else {
                weakSelf.delegate?.show(message: result.description)
                return
            }
            weakSelf.delegate?.show(error: error)
        })
    }
    
    func stopRequest() {
        self.disposable?.dispose()
    }
}

//MARK: - Realm Notifications
extension ShapeViewModel {
    func setupRealmNotifications() {
        self.notificationToken = self.results.observe({ [unowned self] (changes) in
            switch changes {
            case .initial: break
            case .update(_, deletions:  _, insertions: _, modifications: _):
                self.configureTitle()
                self.configureFilds()
                self.delegate?.updateTableView()
                break
            case .error(let error):
                fatalError("\(error)")
                break
            }
        })
    }
}

