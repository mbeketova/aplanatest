//
//  FieldViewModel.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation

protocol FieldViewModelDelegate: class {
    func updateConcreteValue(name: String, value: String)
}

final class FieldViewModel: ViewModelProtocol {
    let name: String
    let title: String
    let type: ValueType
    var concreteValue: String = ""
    var selectedListValue: String = "Выбрать значение"
    weak var delegate: FieldViewModelDelegate?
    private var value: BaseValue?
    private let model: Field
    
    init(model: Field) {
        self.model = model
        self.name = model.name
        self.title = model.title
        self.type = model.type
        self.value = model.value
        configureConcreteValue()
    }
}

//MARK: - Configure
private extension FieldViewModel {
    func configureConcreteValue() {
        guard let value = self.value else { return }
        switch self.type {
        case .text:
            self.concreteValue = (value as! TextValue).concreteValue
        case .list:
            guard (value as? ListValue)?.values != nil else { return }
            self.concreteValue = ""
        case .numeric:
            self.concreteValue = (value as! NumericValue).concreteValue == 0 ? "" : (value as! NumericValue).concreteValue.description
        }
    }
}
