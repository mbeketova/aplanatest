//
//  SelectedViewModel.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation

protocol SelectedViewModelDelegate: class {
    func selected(name: String, value: String)
}

final class SelectedViewModel {
    var values: [String] = []
    weak var delegate: SelectedViewModelDelegate?
    private let name: String
    init(name: String, values: [String]) {
        self.values = values
        self.name = name
    }
    
    func selectedValue(at index: Int) {
        self.delegate?.selected(name: self.name, value: values[index])
    }
}
