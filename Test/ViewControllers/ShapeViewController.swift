//
//  ShapeViewController.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import UIKit
import SnapKit

final class ShapeViewController: UIViewController {
    private let tableView = UITableView(frame: .zero, style: .grouped)
    private let sendButton = UIButton()
    private let viewModel = ShapeViewModel()
    private var indicatorView: ActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

//MARK: - UITableViewDelegate
extension ShapeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let field = self.viewModel.fildViewModels[indexPath.row]
        guard field.type == .list else { return }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectedViewController") as! SelectedViewController
        vc.viewModel = self.viewModel.selectedViewModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ListTableViewCell.height
    }
}

//MARK: - UITableViewDataSource
extension ShapeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.fildViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return cell(tableView:tableView, cellForRowAt: indexPath)
    }
}

//MARK: - ActivityDelegate
extension ShapeViewController: ActivityDelegate {
    func stopAnimating() {
        self.indicatorView.stopAnimating() { [weak self] in
            guard let weakSelf = self else { return }
            weakSelf.viewModel.stopRequest()
        }
    }
}

//MARK: - ShapeViewModelDelegate
extension ShapeViewController: ShapeViewModelDelegate {
    func updateTableView() {
        setupTitle()
        self.tableView.reloadData()
        self.indicatorView.stopAnimating {}
    }
    
    func show(error: Error) {
        self.showAlert(title: error.title!, message: error.message)
        self.indicatorView.stopAnimating {}
    }
    
    func show(message: String) {
        self.showAlert(title: "Успех", message: message)
        self.indicatorView.stopAnimating {}
    }
}

//MARK: - Requests
private extension ShapeViewController {
    func loadShape() {
        self.viewModel.loadShape()
        self.indicatorView.startAnimating()
    }
    
    @objc func sendAction() {
        self.indicatorView.startAnimating()
        self.viewModel.sendValues()
    }
}

//MARK: - Setup
private extension ShapeViewController {
    func setup() {
        subscribeViewModel()
        setupTableView()
        setupButton()
        configureTableView()
        setupTitle()
        setupActivityIndicatorView()
        loadShape()
    }
    
    func subscribeViewModel() {
        self.viewModel.delegate = self
    }
    
    func setupTableView() {
        self.tableView.backgroundColor = .white
        self.view.addSubview(self.tableView)
        
        self.tableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            if #available(iOS 11.0, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
                make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            } else {
                make.top.equalTo(self.topLayoutGuide.snp.bottom)
                make.bottom.equalTo(self.bottomLayoutGuide.snp.top)
            }
        }
    }
    
    func setupButton() {
        self.sendButton.setTitle("Отправить", for: .normal)
        self.sendButton.backgroundColor = .blue
        self.sendButton.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
        self.view.addSubview(self.sendButton)
        self.view.bringSubview(toFront: self.sendButton)
        
        self.sendButton.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.height.equalTo(Constants.UI.buttonHeight)
            if #available(iOS 11.0, *) {
                make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            } else {
                make.bottom.equalTo(self.bottomLayoutGuide.snp.top)
            }
        }
    }
    
    func setupTitle() {
        self.title = self.viewModel.title
    }
    
    func setupActivityIndicatorView() {
        self.indicatorView = ActivityIndicatorView(frame: CGRect(x: (UIScreen.main.bounds.width - Constants.UI.indicatorSize.width)/2,
                                                                 y: (UIScreen.main.bounds.height - Constants.UI.indicatorSize.height)/2,
                                                                 width: Constants.UI.indicatorSize.width,
                                                                 height: Constants.UI.indicatorSize.height))
        self.view.addSubview(self.indicatorView)
        self.view.bringSubview(toFront: self.indicatorView)
        self.indicatorView.delegate = self
    }
}

//MARK: - Configure
private extension ShapeViewController {
    func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(TextTableViewCell.self, forCellReuseIdentifier: TextTableViewCell.reuseIdentifier)
        self.tableView.register(NumericTableViewCell.self, forCellReuseIdentifier: NumericTableViewCell.reuseIdentifier)
        self.tableView.register(ListTableViewCell.self, forCellReuseIdentifier: ListTableViewCell.reuseIdentifier)
    }
    
    func cell(tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let field = self.viewModel.fildViewModels[indexPath.row]
        switch field.type {
        case .list:
            let cell = tableView.dequeue(indexPath: indexPath) as ListTableViewCell
            cell.bind(viewModel: field)
            return cell
        case .numeric:
            let cell = tableView.dequeue(indexPath: indexPath) as NumericTableViewCell
            cell.bind(viewModel: field)
            return cell
        case .text:
            let cell = tableView.dequeue(indexPath: indexPath) as TextTableViewCell
            cell.bind(viewModel: field)
            return cell
        }
    }
}

//MARK: - Alert
private extension ShapeViewController {
    func showAlert(title: String, message : String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (UIAlertAction) in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

