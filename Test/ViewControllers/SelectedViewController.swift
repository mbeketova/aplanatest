//
//  SelectedViewController.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import UIKit

final class SelectedViewController: UIViewController {
    private let tableView = UITableView(frame: .zero, style: .grouped)
    var viewModel: SelectedViewModel = SelectedViewModel(name: "", values: [])
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        configureTableView()
    }

}

//MARK - UITableViewDelegate
extension SelectedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SelectedListTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.selectedValue(at: indexPath.row)
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK - UITableViewDataSource
extension SelectedViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(indexPath: indexPath) as SelectedListTableViewCell
        cell.bind(viewModel: self.viewModel.values[indexPath.row])
        return cell
    }
}

//MARK: - Setup
private extension SelectedViewController {
    func setupTableView() {
        self.view.addSubview(self.tableView)
        self.tableView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            if #available(iOS 11.0, *) {
                make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
                make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            } else {
                make.top.equalTo(self.topLayoutGuide.snp.bottom)
                make.bottom.equalTo(self.bottomLayoutGuide.snp.top)
            }
        }
    }
    
    func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(SelectedListTableViewCell.self, forCellReuseIdentifier: SelectedListTableViewCell.reuseIdentifier)
    }
}
