//
//  JsonParser.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation

struct JsonParser {
    static func parse(data: Data) -> AnyObject {
        if let object = try? JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as AnyObject {
            return object
        }
        return Error.corruptedDataError as AnyObject
    }
}

