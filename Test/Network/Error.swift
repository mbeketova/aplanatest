//
//  Error.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation

enum ErrorAttributes: String {
    case title = "title"
    case message = "message"
    case code = "code"
}

enum ErrorType {
    case binding
    case server
    case user
    case cancelled
}

struct Error: Swift.Error {
    let title: String?
    let message: String
    let code: Int
    
    var type: ErrorType {
        switch self.code {
        case 0..<100:
            return .binding
        case 100..<200:
            return .server
        case 102, -999:
            return .cancelled
        default:
            return .user
        }
    }
    
    init(error: NSError) {
        self.title = nil
        self.message = error.localizedDescription
        self.code = error.code
    }
    
    init(code: Int, title: String, message: String) {
        self.code = code
        self.title = title
        self.message = message
    }
    
    static var commonError: Error {
        return Error(code: Constants.ErrorCodes.corruptedData,
                     title: "Что-то пошло не так",
                     message: "Ты уверен, что тебе все это нужно? ")
    }
}

//MARK: - Constants
extension Error {
    static var connectionError: Error {
        return Error(code: Constants.ErrorCodes.connectionLost,
                     title: "Интернет почини",
                     message: "Сейчас бы без сети тестовое задание смотреть")
    }
    static var corruptedDataError: Error {
        return Error(code: Constants.ErrorCodes.corruptedData,
                     title: "А что если поля заполнить?",
                     message: "Все правильно, все верно, а ребеночек не наш! ")
    }
}

extension NSError {
    var isConnectionLostError: Bool {
        return self.code == Constants.ErrorCodes.connectionLost
    }
}
