//
//  ShapeRouter.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Alamofire
import RealmSwift

protocol NetworkRouterParams {
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters { get }
    var encoding: ParameterEncoding { get }
    var headers: [String: String]? { get }
}

extension NetworkRouterParams {
    var encoding: ParameterEncoding {
        return URLEncoding.methodDependent
    }
    
    var headers: [String: String]? {
        return ["Content-Type" : "application/json", "Accept" : "application/json"]
    }
}

enum ShapeRouter: URLRequestConvertible {
    case meta()
    case data(selectedValue: [[String:String]])
}

extension ShapeRouter: NetworkRouterParams {
    var path: String {
        switch self {
        case .meta:
            return "meta"
        case .data:
            return "data"
        }
    }
    
    var method: HTTPMethod {
        return .post
    }
    
    var parameters: Parameters {
        switch self {
        case .data(let values):
            let params = ["form" : values]
            return params
        default:
            return [:]
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .data:
            return JSONEncoding()
        default:
            return URLEncoding.methodDependent
        }
    }
}
