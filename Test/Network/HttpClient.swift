//
//  HttpClient.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift
import Result

final class HttpClient {
    static let shared = HttpClient()
    
    private let reachabilityManager = NetworkReachabilityManager(host: Constants.Network.baseUrl.absoluteString)!
    private var retryQueue: [EnqueuedRequest] = []
    
    init() {
        startReachabilityManager()
    }
    
    deinit {
        stopReachabilityManager()
    }
    
    func load(request: URLRequestConvertible & NetworkRouterParams, qos: DispatchQoS.QoSClass = .default) -> SignalProducer<AnyObject, Error> {
        return SignalProducer { sink, disposable in
            sessionManager.request(request).response(queue: DispatchQueue.global(qos: qos)) { response in
                guard response.error?.isConnectionLostError != true else {
                    self.handleConnectionError(for: request, observer: sink); return
                }
                guard let statusCode = response.response?.statusCode else { sink.sendInterrupted(); return }
                switch(statusCode) {
                case 200...205:
                    self.handleResponse(for: response.data!, observer: sink)
                case 400..<600:
                    self.handleErrorResponse(for: response.data!, observer: sink)
                default:
                    sink.sendInterrupted()
                }
            }
        }
    }
    
    func cancelRequests(for path: String) {
        sessionManager.cancelTasks(for: path)
    }
}

//MARK: - Retry
private class EnqueuedRequest {
    let request: URLRequestConvertible & NetworkRouterParams
    weak var observer: Signal<AnyObject, Error>.Observer?
    
    init(request: URLRequestConvertible & NetworkRouterParams, observer: Signal<AnyObject, Error>.Observer) {
        self.request = request
        self.observer = observer
    }
}

private extension HttpClient {
    func startSuspendedRequests() {
        self.retryQueue.forEach { (item) in
            guard let observer = item.observer else { return }
            self.load(request: item.request).start(observer)
        }
        self.retryQueue.removeAll()
    }
    
    func handleConnectionError(for request: URLRequestConvertible & NetworkRouterParams, observer: Signal<AnyObject, Error>.Observer) {
        guard request.method == .get else {
            observer.send(error: Error.connectionError)
            return
        }
        self.retryQueue.append(EnqueuedRequest(request: request, observer: observer))
    }
}

//MARK: - Reachability
private extension HttpClient {
    func startReachabilityManager() {
        self.reachabilityManager.listener = self.handleNetworkReachability
        self.reachabilityManager.startListening()
    }
    
    func stopReachabilityManager() {
        self.reachabilityManager.stopListening()
    }
    
    func handleNetworkReachability(status: Alamofire.NetworkReachabilityManager.NetworkReachabilityStatus) {
        switch status {
        case .notReachable: break
        case .reachable(_), .unknown:
            startSuspendedRequests()
        }
    }
}

//MARK: - Response Handlers
private extension HttpClient {
    func handleResponse(for data: Data, observer: Signal<AnyObject, Error>.Observer) {
        guard data.count != 0 else { observer.sendCompleted(); return }
        let parsingResult = JsonParser.parse(data: data)
        
        guard let info = parsingResult as? [String : AnyObject] else {
            guard let error = parsingResult as? Error else {
                observer.send(error: .corruptedDataError)
                return
            }
            observer.send(error: error)
            return
        }
        observer.send(value: info as AnyObject)
        observer.sendCompleted()
    }
    
    func handleErrorResponse(for data: Data, observer: Signal<AnyObject, Error>.Observer) {
        observer.send(error: Error.commonError)
    }
}

//MARK: - Error
fileprivate extension Swift.Error {
    var isConnectionLostError: Bool {
        let error = self as NSError
        return error.isConnectionLostError
    }
}
