//
//  SessionManager.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import Foundation
import Alamofire

let sessionManager: Alamofire.SessionManager = createManager()

private func createManager() -> Alamofire.SessionManager {
    let configuration = URLSessionConfiguration.default
    configuration.httpMaximumConnectionsPerHost = 1
    configuration.urlCache = nil
    configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
    
    let manager = Alamofire.SessionManager(configuration: configuration)
    manager.delegate.taskWillPerformHTTPRedirection = httpRedirectionHandler
    
    return manager
}

private func httpRedirectionHandler(session: URLSession,
                                        task: URLSessionTask,
                                        response: HTTPURLResponse,
                                        request: URLRequest) -> URLRequest? {
    let baseAddress = Constants.Network.baseUrl.absoluteString
    guard request.url?.absoluteString.contains(baseAddress) == true else { return nil }
    return request
}
