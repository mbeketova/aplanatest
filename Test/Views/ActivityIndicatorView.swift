//
//  ActivityIndicatorView.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import UIKit
import CoreGraphics
import Foundation
import SnapKit

protocol ActivityDelegate: class {
    func stopAnimating()
}

final class ActivityIndicatorView: UIView {
    private let progressArcLayer: CAShapeLayer = CAShapeLayer()
    private let cancelButton: UIButton = UIButton()
    private let rotationAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
    private let doneAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
    private var completion: (() -> Void)?
    weak var delegate: ActivityDelegate?
    var lineWidth: CGFloat = 4 {
        didSet {
            self.progressArcLayer.lineWidth = lineWidth
        }
    }
    
    var padding: CGFloat = 22 {
        didSet {
            let arcRect = CGRect(x: padding, y: padding, width: bounds.size.width - padding * 2, height: bounds.size.height - padding * 2)
            self.progressArcLayer.path = UIBezierPath(ovalIn: arcRect).cgPath
        }
    }
    
    var shouldShowDoneCheckmark: Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func startAnimating() {
        self.layer.addSublayer(progressArcLayer)
        self.progressArcLayer.add(rotationAnimation, forKey: "transform.rotation.z")
        self.cancelButton.isHidden = false
    }
    
    func stopAnimating(completion: @escaping (() -> Void)) {
        self.cancelButton.isHidden = true
        progressArcLayer.removeAllAnimations()
        progressArcLayer.removeFromSuperlayer()
        self.completion = completion
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            self.completion?()
        }
    }
}

//MARK: - Setup
private extension ActivityIndicatorView {
    func setup() {
        setupRotationAnimation()
        setupCancelButton()
    }
    
    func setupRotationAnimation() {
        let size = bounds.size
        let arcRect = CGRect(x: padding, y: padding, width: size.width - padding * 2, height: size.height - padding * 2)
        self.progressArcLayer.path = UIBezierPath(ovalIn: arcRect).cgPath
        self.progressArcLayer.lineCap = kCALineCapRound
        self.progressArcLayer.fillColor = UIColor.clear.cgColor
        self.progressArcLayer.strokeColor = UIColor.blue.cgColor
        self.progressArcLayer.lineWidth = lineWidth
        self.progressArcLayer.strokeStart = 0
        self.progressArcLayer.strokeEnd = 0.5;
        self.progressArcLayer.frame = bounds;
        
        self.rotationAnimation.toValue = M_PI * 2
        self.rotationAnimation.duration = 1
        self.rotationAnimation.isCumulative = true
        self.rotationAnimation.repeatCount = HUGE
        self.rotationAnimation.isRemovedOnCompletion = false
        self.rotationAnimation.fillMode = kCAFillModeForwards
        self.rotationAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    }
    
    func setupCancelButton() {
        self.cancelButton.setTitle("", for: .normal)
        self.cancelButton.setImage(UIImage(named: "cancel")?.maskWith(color: .blue), for: .normal)
        self.cancelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        self.cancelButton.isHidden = true
        self.addSubview(self.cancelButton)
        self.bringSubview(toFront: self.cancelButton)
        self.cancelButton.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
    }
}

private extension ActivityIndicatorView {
    @objc func cancelAction() {
        self.cancelButton.isHidden = true
        self.delegate?.stopAnimating()
    }
}

extension ActivityIndicatorView : CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        guard flag else {return}
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            self.completion?()
        }
    }
}
