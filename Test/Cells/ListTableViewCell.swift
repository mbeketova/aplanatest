//
//  ListTableViewCell.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import UIKit
import SnapKit

final class ListTableViewCell: UITableViewCell {
    private let titleLabel = UILabel()
    private let valueLabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - Setup
extension ListTableViewCell {
    func setup() {
        setupAccessoryType()
        setupValueLabel()
        setupTitleLabel()
    }
    
    func setupAccessoryType() {
        self.accessoryType = .disclosureIndicator
    }
    
    func setupTitleLabel() {
        self.contentView.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(self.contentView).offset(Constants.UI.horizontalMargin)
            make.right.equalTo(self.valueLabel.snp.left)
        }
    }
    
    func setupValueLabel() {
        self.contentView.addSubview(self.valueLabel)
        self.valueLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.equalTo(self.contentView).offset(Constants.UI.horizontalMargin)
            make.width.equalTo(self.contentView.bounds.width/2 + 10)
        }
    }
}

extension ListTableViewCell: TableViewCellProtocol {
    static var height: CGFloat {
        return Constants.UI.cellHeight
    }
    
    func bind(viewModel: FieldViewModel) {
        self.titleLabel.text = viewModel.title
        self.valueLabel.text = viewModel.concreteValue.isEmpty ? "Выбрать значение" : viewModel.concreteValue
    }
}
