//
//  NumericTableViewCell.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import UIKit

final class NumericTableViewCell:  UITableViewCell, Reusable  {
    private let titleLabel = UILabel()
    private let textField = UITextField()
    private var viewModel: FieldViewModel!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupTextField()
        setupTitleLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

//MARK: - Setup
extension NumericTableViewCell {
    func setupTitleLabel() {
        self.contentView.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self.contentView).offset(Constants.UI.horizontalMargin)
            make.top.bottom.equalToSuperview()
            make.right.equalTo(self.textField).offset(5)
        }
    }
    
    func setupTextField() {
        self.textField.delegate = self
        self.textField.keyboardType = .decimalPad
        self.contentView.addSubview(self.textField)
        self.textField.snp.makeConstraints { (make) in
            make.right.equalTo(self.contentView).offset(Constants.UI.horizontalMargin)
            make.top.bottom.equalToSuperview()
            make.width.equalTo(self.contentView.bounds.width/2 + Constants.UI.widthRightDelta)
        }
    }
}

//MARK: - TableViewCellProtocol
extension NumericTableViewCell: TableViewCellProtocol {
    func bind(viewModel: FieldViewModel) {
        self.viewModel = viewModel
        self.titleLabel.text = viewModel.title
        self.textField.placeholder = viewModel.concreteValue.isEmpty ? "Введите значение" : viewModel.concreteValue
    }
    
    static var height: CGFloat {
        return Constants.UI.cellHeight
    }
}

//MARK: - UITextFieldDelegate
extension NumericTableViewCell: UITextFieldDelegate {
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        self.viewModel.concreteValue = text
        self.viewModel.delegate?.updateConcreteValue(name: self.viewModel.name, value: self.viewModel.concreteValue)
    }
}
