//
//  SelectedListTableViewCell.swift
//  Test
//
//  Created by Mariya on 27.04.2018.
//  Copyright © 2018 Mariya. All rights reserved.
//

import UIKit

final class SelectedListTableViewCell: UITableViewCell, Reusable {
    private let titleLabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupTitleLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - TableViewCellProtocol
extension SelectedListTableViewCell: TableViewCellProtocol {
    func bind(viewModel: String) {
        self.titleLabel.text = viewModel
    }
    
    static var height: CGFloat {
        return Constants.UI.cellHeight
    }
}

//MARK: - Setup
private extension SelectedListTableViewCell {
    func setupTitleLabel() {
        self.contentView.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.right.equalTo(self.contentView).offset(Constants.UI.horizontalMargin)
        }
    }
}

